package com.askartec.lib.logger;

public final class LogLevels {
    private LogLevels() {

    }

    public static final Integer FATAL  = 1;
    public static final Integer ERROR  = 2;
    public static final Integer WARN   = 3;
    public static final Integer INFO   = 4;
    public static final Integer DEBUG  = 5;
    public static final Integer TRACE  = 6;
}
