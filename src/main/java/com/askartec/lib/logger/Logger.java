package com.askartec.lib.logger;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.querybuilder.Insert;
import com.datastax.driver.core.querybuilder.QueryBuilder;

import java.util.Date;

public class Logger {
    private Cluster cluster;
    private Session session;
    private Integer logLevel;
    private String componentName;
    private String keyspace;
    private String tableName;
    private Date time;

    public Logger(String dbAddress, String keyspace, String tableName, Integer logLevel, String componentName) {
        this.logLevel = logLevel;
        this.componentName = componentName;
        this.keyspace = keyspace;
        this.tableName = tableName;
        this.time = new Date();
        cluster = Cluster.builder()
                .addContactPoint(dbAddress)
                .build();
        session = cluster.connect(keyspace);
    }

    public static void main(String[] args) {
        return;
    }

    private String buildQuery(String taskId, Integer logLevel, String msg) {
        Insert insert = QueryBuilder.insertInto(keyspace, tableName)
                .value("component",     componentName)
                .value("level",         logLevel)
                .value("date_created",  time.getTime())
                .value("task_id",       taskId)
                .value("message",       msg);

        return insert.toString();
    }

    public void fatal(String taskId, String msg) {
        if (logLevel < LogLevels.FATAL)
            return;

        try {
            this.session.execute(buildQuery(taskId, LogLevels.FATAL, msg));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void error(String taskId, String msg) {
        if (logLevel < LogLevels.ERROR)
            return;

        try {
            this.session.execute(buildQuery(taskId, LogLevels.ERROR, msg));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void warn(String taskId, String msg) {
        if (logLevel < LogLevels.WARN)
            return;

        try {
            this.session.execute(buildQuery(taskId, LogLevels.WARN, msg));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void info(String taskId, String msg) {
        if (logLevel < LogLevels.INFO)
            return;

        try {
            this.session.execute(buildQuery(taskId, LogLevels.INFO, msg));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void debug(String taskId, String msg) {
        if (logLevel < LogLevels.DEBUG)
            return;

        try {
            this.session.execute(buildQuery(taskId, LogLevels.DEBUG, msg));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void trace(String taskId, String msg) {
        if (logLevel < LogLevels.TRACE)
            return;

        try {
            this.session.execute(buildQuery(taskId, LogLevels.TRACE, msg));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void shutdown() {
        cluster.close();
    }
}
